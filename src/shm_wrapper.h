#include <napi.h>
#include "shm.h"

class SHMWrapper : public Napi::ObjectWrap<SHMWrapper> {
 public:
  static Napi::Object Init(Napi::Env env, Napi::Object exports);
  SHMWrapper(const Napi::CallbackInfo& info);

 private:
  static Napi::FunctionReference constructor;
  Napi::Value Get(const Napi::CallbackInfo& info);
  Napi::Value Size(const Napi::CallbackInfo& info);
  void Clean(const Napi::CallbackInfo& info);
  SHM *shm_;
};
