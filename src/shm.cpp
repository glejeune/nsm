#include "shm.h"

SHM::SHM(std::string pathname, size_t size) {
  this->size = size;
  this->path = (char*)malloc(sizeof(char) * (pathname.size() + 1));
  strcpy(this->path, pathname.c_str());

  this->fd = shm_open(this->path, O_RDWR | O_CREAT, S_IRUSR | S_IWUSR);
  if (this->fd == -1) {
    throw "shm_open error";
  }

  int res = ftruncate(this->fd, size);
  if (res == -1) {
    throw "ftruncate error";
  }

  this->addr = mmap(NULL, size, PROT_WRITE, MAP_SHARED, fd, 0);
  if (addr == MAP_FAILED) {
    throw "mmap error";
  }
}

SHM::~SHM() {
  this->clean();
}

void SHM::clean() {
  if (this->addr != NULL || this->fd != 0) {
    int res = munmap(this->addr, this->size);
    if (res == -1) {
      throw "munmap error";
    }
    this->addr = NULL;

    this->fd = shm_unlink(this->path);
    if (this->fd == -1) {
      throw "shm_unlink error";
    }
  }
}
