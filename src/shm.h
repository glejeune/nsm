#include <string>
#include <cstring>
#include <sys/mman.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/types.h>
#include <stdio.h>
#include <string.h>

class SHM {
  public:
    SHM(std::string pathname, size_t size);
    ~SHM();
    void clean();

    size_t size;
    void* addr;

  private:
    int fd;
    char *path;
};
