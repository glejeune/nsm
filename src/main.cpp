#include "shm_wrapper.h"

Napi::Object InitAll(Napi::Env env, Napi::Object exports) {
  return SHMWrapper::Init(env, exports);
}

NODE_API_MODULE(NODE_GYP_MODULE_NAME, InitAll)
