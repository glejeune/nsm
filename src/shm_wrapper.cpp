#include "shm_wrapper.h"

Napi::FunctionReference SHMWrapper::constructor;

Napi::Object SHMWrapper::Init(Napi::Env env, Napi::Object exports) {
  Napi::HandleScope scope(env);

  Napi::Function func = DefineClass(env, "SHMWrapper", {
      InstanceMethod("get", &SHMWrapper::Get),
      InstanceMethod("size", &SHMWrapper::Size),
      InstanceMethod("clean", &SHMWrapper::Clean),
      });

  constructor = Napi::Persistent(func);
  constructor.SuppressDestruct();

  exports.Set("SHMWrapper", func);
  return exports;
}

SHMWrapper::SHMWrapper(const Napi::CallbackInfo& info) : Napi::ObjectWrap<SHMWrapper>(info)  {
  Napi::Env env = info.Env();
  Napi::HandleScope scope(env);

  int length = info.Length();
  if (length != 2 || !info[0].IsString() || !info[1].IsNumber()) {
    Napi::TypeError::New(env, "Invalid parameters").ThrowAsJavaScriptException();
  }

  Napi::String pathname = info[0].As<Napi::String>();
  Napi::Number size = info[1].As<Napi::Number>();

  try {
    this->shm_ = new SHM(pathname.Utf8Value(), size.Int32Value());
  } catch(...) {
    this->shm_ = NULL;
    Napi::TypeError::New(env, "Can't initialize shared memory").ThrowAsJavaScriptException();
  }
}

Napi::Value SHMWrapper::Get(const Napi::CallbackInfo& info) {
  Napi::Env env = info.Env();
  Napi::HandleScope scope(env);

  return Napi::ArrayBuffer::New(env, this->shm_->addr, this->shm_->size);
}

Napi::Value SHMWrapper::Size(const Napi::CallbackInfo& info) {
  Napi::Env env = info.Env();
  Napi::HandleScope scope(env);

  return Napi::Number::New(env, 1.0 * this->shm_->size);
}

void  SHMWrapper::Clean(const Napi::CallbackInfo& info) {
  Napi::Env env = info.Env();
  Napi::HandleScope scope(env);

  try {
    if (this->shm_) {
      delete this->shm_;
    }
  } catch(...) {
    Napi::TypeError::New(env, "Failed to free shared memory").ThrowAsJavaScriptException();
  }

  return;
}
