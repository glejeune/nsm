'use strict';

const _should = require('should');
const cluster = require('cluster');
const NSM = require('..');

function testCluster(type) {
  const sharedMemory = new NSM('/hello');

  cluster.setupMaster({
    exec: `${__dirname}/cluster/nsm-worker.js`,
    args: [],
  });

  cluster.on('fork', worker => {
    worker.on('message', message => {
      switch (message.ack) {
        case 'ready':
          worker.send({ ack: type });
          break;
        case 'stored':
          sharedMemory.get().should.eql(message.data);
          break;
        default:
          break;
      }
    });
  });
  cluster.fork();
}

describe('NSM-cluster', () => {
  it('should retrieve the string set in the worker', () => {
    testCluster('string');
  });

  it('should retrieve the integer set in the worker', () => {
    testCluster('integer');
  });

  it('should retrieve the float set in the worker', () => {
    testCluster('float');
  });

  it('should retrieve the object set in the worker', () => {
    testCluster('object');
  });
});
