'use strict';

const NSM = require('../..');

function store(data, shm) {
  shm.set(data);
  process.send({ ack: 'stored', data });
}

const sharedMemory = new NSM('/hello');

process.on('message', message => {
  switch (message.ack) {
    case 'exit':
      process.disconnect();
      break;
    case 'string':
      store('Hello World', sharedMemory);
      break;
    case 'integer':
      store(42, sharedMemory);
      break;
    case 'float':
      store(42.1, sharedMemory);
      break;
    case 'object':
      store({ string: 'string', integer: 42, float: 42.1 }, sharedMemory);
      break;
    default:
      break;
  }
});

process.send({ ack: 'ready' });
