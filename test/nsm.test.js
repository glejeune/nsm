'use strict';

const _should = require('should');
const NSM = require('..');

describe('NSM', () => {
  it('should set and retrieve a string', () => {
    const sharedString = new NSM('/test');
    sharedString.set('Hello World');
    sharedString.get().should.eql('Hello World');
  });

  it('should set and retrieve an integer', () => {
    const sharedInteger = new NSM('/test');
    sharedInteger.set(42);
    sharedInteger.get().should.eql(42);
  });

  it('should set and retrieve a float', () => {
    const sharedFloat = new NSM('/test');
    sharedFloat.set(42.1);
    sharedFloat.get().should.eql(42.1);
  });

  it('should set and retrieve an object', () => {
    const sharedObject = new NSM('/test');
    const object = { string: 'string', integer: 42, float: 42.1 };
    sharedObject.set(object);
    sharedObject.get().should.eql(object);
  });
});
