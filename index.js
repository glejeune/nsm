'use strict';

const { SHMWrapper } = require('./build/Release/shm.node');

class NSM {
  constructor(name) {
    this._name = name;
  }

  get() {
    const shmSize = new SHMWrapper(`${this._name}::__size__`, 32);
    const size = new DataView(shmSize.get(), 0).getUint32(1);
    const shmData = new SHMWrapper(`${this._name}::__data__`, size * 2);
    const bufView = new Uint16Array(shmData.get());
    const data = String.fromCharCode.apply(null, bufView);
    return JSON.parse(data);
  }

  set(object) {
    this.clean();

    const data = JSON.stringify(object);

    this._shmData = new SHMWrapper(`${this._name}::__data__`, data.length * 2);
    const bufView = new Uint16Array(this._shmData.get());
    for (let i = 0, strLen = data.length; i < strLen; i++) {
      bufView[i] = data.charCodeAt(i);
    }

    this._shmSize = new SHMWrapper(`${this._name}::__size__`, 32);
    new DataView(this._shmSize.get(), 0).setUint32(1, data.length);
  }

  clean() {
    if (this._shmData) {
      this._shmData.clean();
    }
    if (this._shmSize) {
      this._shmSize.clean();
    }
  }
}

module.exports = NSM;
